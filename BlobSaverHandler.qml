import QtQuick 2.4
import QtWebSockets 1.0
import BlobSaver 2.0

Item {
    signal fileDownloaded(string path)

    WebSocketServer {
        id: server
        listen: true
        port: 12345

        onClientConnected: {
            console.log('BlobSaver: websocket server connected');

            webSocket.onTextMessageReceived.connect(function(base64data) {
                console.log('BlobSaver: createObjectURL handler');
                var path = BlobSaver.write(base64data);
                fileDownloaded(path);
            });
        }

        onErrorStringChanged: {
            console.log('BlobSaver: websocket server error', errorString);
        }
    }
}
