import QtQuick 2.9
import QtWebEngine 1.7

WebEngineScript {
    injectionPoint: WebEngineScript.DocumentCreation
    sourceUrl: Qt.resolvedUrl('blobsaver.js')
    worldId: WebEngineScript.MainWorld
}
