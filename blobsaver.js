console.log('BlobSaver: start injection');

function initWebSocket() {
    window.blobsaverSocket = new WebSocket('ws://localhost:12345');

    window.blobsaverSocket.onclose = function() {
        console.error('BlobSaver: websocket closed');
    };

    window.blobsaverSocket.onerror = function(error) {
        console.error('BlobSaver: websocket', error);
    };

    window.blobsaverSocket.onopen = function() {
        console.log('BlobSaver: websocket opened');
    };
}

window.addEventListener('load', initWebSocket, false);

window.URL.createObjectURL = function(obj) {
    // TODO check if the obj is a blob

    console.log('BlobSaver: createObjectURL interceptor');

    var reader = new FileReader();
    reader.readAsDataURL(obj);
    reader.onloadend = function() {
        console.log('BlobSaver: createObjectURL sending message');
        window.blobsaverSocket.send(reader.result);
    };

    throw 'stop'; // Throw an error here to stop execution (continuing execution would likely result in an error from the download manager)
};

console.log('BlobSaver: end injection');
